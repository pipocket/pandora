<div id="nav" class="pure-u hidden-mobile">
    <a href="#" class="alternate hidden-desktop">&#9776;</a>

    <ul class="unstyled">
        <li>
            <a href="<?php echo $this->createUrl('/movies/detail') ?>" class="pure-button pure-button-secondary pure-button-small" data-link-ajax="true" data-target="content">Adicionar Filme</a>
        </li>
        <li class="interval"></li>
        <li>
            <a href="<?php echo $this->createUrl('/movies') ?>" class="default active">Filmes</a>
        </li>
        <li>
            <a href="<?php echo $this->createUrl('/cast') ?>" class="default">Atores e Diretores</a>
        </li>
        <li>
            <a href="<?php echo $this->createUrl('/genres') ?>" class="default">Gêneros</a>
        </li>
        <li class="interval"></li>
        <li>
            <label>Filtro</label>

            <form action="<?php echo $this->createUrl('/movies/list') ?>" action="get" name="movieFilter">
                <ul class="unstyled">
                    <li><input type="text" class="text" placeholder="Buscar filme" name="term"></li>
                    <li><label><input type="checkbox" name="filter[]" value="no-poster"/> Sem poster</label></li>
                    <li><label><input type="checkbox" name="filter[]" value="incomplete"/> Incompletos</label></li>
                    <li><label><input type="checkbox" name="filter[]" value="awaiting-approval"/> Aguardando aprovação</label></li>
                </ul>
            </form>
        </li>
    </ul>
</div>
<div id="list" class="pure-u" data-action="<?php echo $this->createUrl('/movies/list') ?>" data-page="2">
    <ul class="unstyled">
        <?php $this->renderPartial('list', array('movies'=>$movies)) ?>
    </ul>
    <a href="#" class="load-more hidden-desktop" rel="more">Carregar mais</a>
</div>
<div id="content" class="pure-u hidden-mobile" data-container="content">
</div>