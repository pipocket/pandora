<li class="clearfix <?php echo $movie->itemStatus ?>" id="movie-<?php echo $movie->id ?>">
    <a href="<?php echo $this->createUrl('/movies/detail/'.$movie->id) ?>" class="overlay" data-ajax="true"></a>
    <span class="image">
    	<?php if ($movie->poster): ?>
			<img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $this->getImage($movie->poster, 'small') ?>" width="50">
    	<?php endif ?>
    </span>
    <span class="info">
        <p class="ttl"><?php echo $movie->name ?></p>
        <p class="subttl"><?php echo $movie->original_name ?></p>
        <p class="subttl">Direção: Jorge de Paula</p>
        <p class="obs">
            <?php if ($movie->itemStatus=='incomplete'): ?>
            Incompleto
            <?php elseif($movie->itemStatus=='publish'): ?>
            Aguardando publicação
            <?php endif ?>
        </p>
    </span>
</li>