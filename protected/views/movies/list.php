<?php foreach ($movies as $movie): ?>
<?php $this->renderPartial('item-list', array('movie'=>$movie)) ?>
<?php endforeach ?>
<?php if (!$movies): ?>
<li class="nothing">Nenhum filme encontrado</li>
<?php endif ?>