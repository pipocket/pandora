<form action="<?php echo $this->createUrl('/movies/save/'.$movie->id) ?>" method="post" name="movieForm" data-post-ajax="true" data-action="">
    <div class="header">
        <h3><?php echo $this->defaultValue($movie, 'name', 'Novo Filme') ?></h3>
        <?php if ($movie->name): ?>
        <p>
            Criado em <span><?php echo $this->dateFormat($movie->register, 'd \'de\' MMMM y \'às\' H:mm') ?></span> <i class="break"></i>
            por <span><?php echo $movie->author ? $movie->author->name : 'Desconhecido' ?></span>
        </p>
        <?php endif ?>

        <div class="buttons">
            <input type="button" class="cancel" value="Cancelar" id="cancel" />
            <input type="button" class="remove submitMovieForm" value="Deletar" id="delete" data-confirm-message="Tem certeza que seja deletar este filme?" />

            <?php if ($movie->name): ?>
                <?php if ($movie->status<=1): ?>
                <input type="button" value="Enviar para aprovação" class="submitMovieForm" id="send-to-approve" data-confirm-message="Tem certeza que deseja enviar para aprovação?" data-confirm-message2="A sinopse foi revisada?" />
                <?php elseif ($movie->status==2): ?>
                <input type="button" value="Publicar" class="submitMovieForm" id="publish" data-confirm-message="Tem certeza que deseja publicar?" />
                <?php endif ?>
            <?php endif ?>
            
            <input type="button" value="Salvar" class="submitMovieForm"/>
        </div>
    </div>
    <div class="main">
        <fieldset class="clearfix">
            <div class="col-item">
                <label>Nome</label>
                <input type="text" name="Movie[name]" class="text" value="<?php echo $movie->name ?>" data-url-check="<?php echo $this->createAbsoluteUrl('/movies/checkslug') ?>" data-id="<?php echo $movie->id ?>" />
                <span class="key"><?php echo $movie->slug ?></span>
                <input type="hidden" name="Movie[slug]" value="<?php echo $movie->slug ?>">
            </div>
            <div class="col-item single no-space">
                <label>Nome original</label>
                <input type="text" name="Movie[original_name]" class="text medium" value="<?php echo $movie->original_name ?>" />
                <input type="text" name="Movie[year]" class="text small" value="<?php echo $movie->year ?>" placeholder="Ano" />
            </div>
        </fieldset>
        <fieldset class="clearfix file-in-form">
            <label>Poster</label>
            <a href="#" class="pure-button" rel="clickFile" data-origin="normal-button" data-default-text="Enviar Poster">Enviar Poster</a>
            <?php if ($movie->poster): ?>
                <a href="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $movie->poster ?>" class="pure-button pure-button-secondary" rel="external">Ver Poster</a>
            <?php endif ?>
            <input type="hidden" name="Movie[poster]" value="<?php echo $movie->poster ?>">
        </fieldset>
        <fieldset>
            <div class="col-item small">
                <label>Estréia</label>
                <input type="text" name="Movie[release]" class="text datepicker" data-value="<?php echo $this->dateFormat($movie->release, 'y-MM-dd') ?>" placeholder="Escolha">
            </div>
            <div class="col-item small">
                <label>Duração</label>
                <input type="text" name="Movie[time]" class="text" value="<?php echo $movie->time ?>" placeholder="Ex: 90">
            </div>
            <div class="col-item small no-space">
                <label>Faixa Etária</label>
                <input type="text" name="Movie[age]" class="text" value="<?php echo $movie->age ?>" placeholder="Ex: 12">
            </div>
        </fieldset>
        <fieldset>
            <div class="col-item">
                <label>Gênero</label>
                <?php $this->widget('application.widgets.Tags', array(
                        'fieldName'=>'MovieGenre', 
                        'items'=>$genres, 
                        'selectedItems'=>$movie->genres, 
                        'itemField'=>'name',
                        'htmlOptions'=>array('class'=>'text input-tags', 'autoComplete'=>'off', 'placeholder'=>'Digite o gênero'),
                    )
                ); ?>
            </div>
            <div class="col-item single no-space">
                <label>País</label>
                <?php $this->widget('application.widgets.Tags', array(
                        'fieldName'=>'MovieCountry', 
                        'items'=>$countries, 
                        'selectedItems'=>$movie->countries, 
                        'itemField'=>'name',
                        'htmlOptions'=>array('class'=>'text input-tags', 'autoComplete'=>'off', 'placeholder'=>'Digite o país'),
                    )
                ); ?>
            </div>
        </fieldset>
        <fieldset>
            <div class="col-item">
                <label>Direção</label>
                <?php $this->widget('application.widgets.Tags', array(
                        'fieldName'=>'MovieCast[director]', 
                        'items'=>$directors, 
                        'selectedItems'=>$movie->directors, 
                        'itemField'=>'cast',
                        'itemFieldChildren'=>'name',
                        'htmlOptions'=>array('class'=>'text input-tags', 'autoComplete'=>'off', 'placeholder'=>'Digite o nome do diretor'),
                    )
                ); ?>
            </div>
            <div class="col-item single no-space">
                <label>Atores</label>
                <?php $this->widget('application.widgets.Tags', array(
                        'fieldName'=>'MovieCast[actor]', 
                        'items'=>$actors, 
                        'selectedItems'=>$movie->actors, 
                        'itemField'=>'cast',
                        'itemFieldChildren'=>'name',
                        'htmlOptions'=>array('class'=>'text input-tags', 'autoComplete'=>'off', 'placeholder'=>'Digite o nome do ator/atriz'),
                    )
                ); ?>
            </div>                
        </fieldset>
        <fieldset>
            <label>Sinopse</label>
            <textarea name="Movie[review]"><?php echo $movie->review ?></textarea>
        </fieldset>
        <fieldset class="clearfix review_status">
            <label><input type="radio" name="Movie[review_status]" value="0" <?php if($movie->review_status==0): ?>checked<?php endif; ?>> Não revisada</label>
            <label><input type="radio" name="Movie[review_status]" value="1" <?php if($movie->review_status==1): ?>checked<?php endif; ?>> Revisada</label>
        </fieldset>

        <div class="poster-box">
            <?php if ($movie->poster): ?>
                <img src="<?php echo Yii::app()->params['aws']['s3BaseUrl'] . $movie->poster ?>" alt="">
            <?php endif ?>
            <a href="#" class="pure-button" rel="clickFile" data-default-text="Enviar Poster">Enviar Poster</a>
            <input type="file" class="submitFile hidden" data-show-thumb="true" data-upload-url="<?php echo $this->createAbsoluteUrl('/movies/upload') ?>">
        </div>
    </div>
</form>

<script>
    plugins();
    poster();
</script>