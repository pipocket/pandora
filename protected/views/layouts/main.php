<!DOCTYPE html>
<html class="no-js">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title><?php echo CHtml::encode($this->pageTitle); ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/normalize.min.css" />
        <link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/pure/0.2.0/pure-min.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/main.css" />
        <!-- Datepicker -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/default.css" />
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/default.date.css" />
        <!-- Toastr -->
        <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/public/css/toastr.min.css" />

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/libs.min.js"></script>
    </head>
    <body>
    
        <div class="pure-g-r">
            <div class="pure-u-1-1 clearfix">
                <div id="top" class="pure-u clearfix">
                    <h1>Pandora <span>pipocket.com</span></h1>
                    <?php if (!Yii::app()->user->isGuest): ?>
                    <ul class="unstyled clearfix hidden-mobile">
                        <li><a href="<?php echo $this->createUrl('/dashboard') ?>" class="<?php if($this->id=='dashboard'): ?>active<?php endif; ?>">Página Inicial</a></li>
                        <li><a href="<?php echo $this->createUrl('/movies') ?>" class="<?php if($this->id=='movies'): ?>active<?php endif; ?>">Filmes</a></li>
                    </ul>
                    <a href="#menu" class="menu visible-mobile">&#9776;</a>

                    <div class="user clearfix">
                        <a href="#" class="overlay"></a>
                        <div class="info">
                            <p class="name"><?php echo $this->user->name ?></p>
                            <span>Configurações</span>
                        </div>
                        <div class="image">
                            <img src="<?php echo $this->createAbsoluteUrl('/public/images/eu.jpg') ?>" alt="">
                        </div>
                        <div class="logout">
                            <a href="<?php echo $this->createUrl('/login/logout') ?>" class="icons" title="Sair"><!-- Sair --></a>
                        </div>
                    </div>
                    <?php endif ?>
                </div>
                <?php echo $content; ?>
            </div>
        </div>

        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/plugins.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/main.js"></script>
        <!-- Datepicker -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/picker.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/picker.date.js"></script>
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/translations/pt_BR.js"></script>
        <!-- Toastr -->
        <script src="<?php echo Yii::app()->request->baseUrl; ?>/public/js/toastr.min.js"></script>
    </body>
</html>