<div class="pure-u" id="login">
	<?php $form=$this->beginWidget('CActiveForm', array(
		'id'=>'login-form',
		'enableClientValidation'=>true,
		'clientOptions'=>array(
			'validateOnSubmit'=>true,
		),
	)); ?>
		<div class="row">
			<?php echo $form->textField($model,'username', array('class'=>'user', 'placeholder'=>'Usuário',  'autocomplete'=>'off')); ?>
			<?php echo $form->passwordField($model,'password', array('class'=>'pass', 'placeholder'=>'Senha')); ?>
			<?php echo CHtml::submitButton('Login', array('class'=>'button')); ?>
		</div>
	<?php $this->endWidget(); ?>
</div>
