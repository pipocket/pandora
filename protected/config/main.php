<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.

// Local config file
$local_config_file = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'main-local.php';

// ENV
function uriByEnvironments() {
	$allow = array(
		'pandora.dev.pipocket.com',
		'pandora.pipocket.com',
	);

	if (array_search($_SERVER['SERVER_NAME'], $allow) !== false)
		return 'http://' . $_SERVER['SERVER_NAME'] . '/login';
}

// Config
$main = array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'Pandora - pipocket.com',
	// 'defaultController'=>'dashboard',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'application.widgets.*',
		'application.extensions.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'simples',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'loginUrl'=>array('login'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
			'caseSensitive'=>false,
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<ajax:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>/<id:\d+>/<ajax:\w+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
				// '<module:\w+>/<controller:\w+>/<action:\w+>'=>'<module>/<controller>/<action>',
			),
		),
		'db'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=pipocket',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => 'ppckt!06#13',
			'charset' => 'utf8',
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),

	// using Yii::app()->params['paramName']
	'params'=>array(
		// Security
		'ApplicationKey'=>'##YSTDYIAdsadsaDSA@!#$%&*DSADS&ASD',
		// Amazon Web Service
		'aws'=>array(
			'use'=>true,
			'key'=>'AKIAIMB53NYHYUWHCH2Q',
			'secret'=>'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',
			'bucket'=>'pipocket',
			's3BaseUrl'=>'https://s3-sa-east-1.amazonaws.com/pipocket/',
		),
		'googleSettings'=>array(
			'app_url'=>'http://pandora.pipocket.com',
			'client_id'=>'837817398191.apps.googleusercontent.com', 
			'client_secret'=>'DRzPvhpTpIqZ7oQObq4pXhcm',
			'redirect_uri'=>uriByEnvironments(),
			'scope'=>array(
				"https://www.googleapis.com/auth/userinfo.email",
				"https://www.googleapis.com/auth/userinfo.profile"
			),
			'onlyPipocketUsers'=>true,
			'pipocketDomain'=>'pipocket.com',
		),
		// Site url
		'siteUrl' => 'http://pipocket.com',
		// this is used in contact page
		'adminEmail'=>'webmaster@pipocket.com',
		// paths
		'basePath'=>dirname(dirname(dirname(__FILE__))),
		'rootBasePath'=>dirname(dirname(dirname(dirname(__FILE__)))),
		// site config
		'paginationLimit'=>15,
		'loginDuration' => (3600*24*1), // 1 Day,
	),
);

if ( file_exists($local_config_file) )
	require $local_config_file;

return $main;
