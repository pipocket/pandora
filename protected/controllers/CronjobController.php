<?php

class CronjobController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionRating()
	{
		$rates = Rate::model()->findAll();
		$avaliation = array();
		$status = array('total'=>0, 'errors'=>array());

		foreach($rates as $rate) {
			$avaliation[$rate->movie_id][] = (int)$rate->rating;
		}

		$status['total'] = count($avaliation);

		foreach($avaliation as $movie_id => $values) {
			$note = array_sum($values);
			$votes = count($values);
			$rating = (int)round($note/$votes);

			$data = Movie::model()->findByPk($movie_id);
			$data->rating = $rating;
			$data->evaluated = $votes;

			if(!$data->save())
				$status['errors'][] = array('movie_id'=>$movie_id,'message'=>$data->errors);
		}

		$this->jsonResponse($status);
	}

	public function actionComments()
	{
		$rates = Rate::model()->findAll('comment IS NOT NULL');
		$movies = array();
		$status = array('total'=>0, 'errors'=>array());

		foreach($rates as $rate) {
			$discussion = (int)Discussion::model()->count('rate_id=:rate_id', array(':rate_id'=>$rate->id));

			if(isset($movies[$rate->movie_id]))
				$movies[$rate->movie_id] += (1+$discussion);
			else
				$movies[$rate->movie_id] = (1+$discussion);
		}

		$status['total'] = count($movies);

		foreach($movies as $movie_id => $comments) {
			$data = Movie::model()->findByPk($movie_id);
			$data->commented = $comments;

			if(!$data->save())
				$status['errors'][] = array('movie_id'=>$movie_id, 'message'=>$data->errors);
		}

		$this->jsonResponse($status);
	}
}