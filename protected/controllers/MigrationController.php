<?php

class MigrationController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionMovies()
	{
		$movies = OldMovie::model()->findAll();
		$status = array('total'=>count($movies), 'added'=>0, 'updated'=>0, 'errors'=>array());

		foreach($movies as $movie) {
			$data = Movie::model()->find('name=:name AND original_name=:original_name', array(':name'=>trim($movie->name), ':original_name'=>trim($movie->original_name)));
			$cast = array('actor'=>'', 'director'=>'');
			$genres = '';
			$updated = true;

			if(is_null($data)) {
				$data = new Movie;
				$updated = false;
			}

			$data->name = trim($movie->name);
			$data->original_name = trim($movie->original_name);
			$data->slug = str_replace('_', '-', $movie->key);
			$data->slug = str_replace(array('---', '--'), array('-', '-'), $data->slug);
			$data->age = $movie->age=='L' ? 0 : $movie->age;
			$data->time = trim($movie->time);
			$data->year = $movie->year;
			$data->review = trim($movie->storyline);
			$data->review_status = $movie->storyline_check;
			$data->release = $movie->release;
			$data->status = $movie->status==1 ? 3 : 1;
			$data->register = $movie->date;
			$data->temp_old_id = $movie->id;

			if ($data->status==3) {
				$data->publish = new CDbExpression('NOW()');
			}
			
			if($data->save()) {
				if($updated)
					$status['updated'] += 1;
				else
					$status['added'] += 1;
			}
			else
				$status['errors'][] = array('movie'=>$data->name, 'messages'=>$data->errors);

			foreach($movie->actors as $actor) {
				$cast['actor'] .= $actor->name . ',';
			}

			foreach($movie->directors as $director) {
				$cast['director'] .= $director->name . ',';
			}

			foreach($movie->genders as $genre) {
				$genres .= $genre->description . ',';
			}

			MovieCast::model()->checkAndSave($cast, $data->id);
			MovieGenre::model()->checkAndSave($genres, $data->id);
			MovieCountry::model()->checkAndSave(str_replace('/', ',', $movie->origin), $data->id, 'acronym');
		}

		$this->jsonResponse($status);
	}

	public function actionUsers()
	{
		$users = OldUser::model()->findAll();
		$status = array('total'=>count($users), 'added'=>0, 'updated'=>0, 'errors'=>array());

		foreach($users as $user) {
			$data = User::model()->find('email=:email', array(':email'=>trim($user->email)));
			$updated = true;

			if(is_null($data)) {
				$data = User::model()->find('twitter=:twitter', array(':twitter'=>trim($user->twitter)));

				if(is_null($data)) {
					$data = User::model()->find('facebook=:facebook', array(':facebook'=>trim($user->facebook)));

					if(is_null($data)) {
						$data = new User;
						$updated = false;
					}
				}
			}

			$data->name = $user->name;
			$data->email = $user->email;
			$data->password = $user->password;
			$data->city = $user->location;
			$data->gender = $user->gender;
			$data->birthdate = $user->birthdate;
			$data->facebook = $user->facebook;
			$data->twitter = $user->twitter;
			$data->register = $user->register;
			$data->status = $user->status;
			$data->temp_old_id = $user->id;

			if($data->save()) {
				if($updated)
					$status['updated'] += 1;
				else
					$status['added'] += 1;
			}
			else
				$status['errors'][] = array('movie'=>$data->name, 'messages'=>$data->errors);
		}

		$this->jsonResponse($status);
	}

	public function actionUserMovies()
	{
		UserMovie::model()->deleteAll();
		$ums = OldUserMovie::model()->findAll();
		$status = array('total'=>count($ums), 'errors'=>array());

		foreach($ums as $um) {
			$movie = Movie::model()->find('temp_old_id=:temp_old_id', array(':temp_old_id'=>$um->movie_id));
			$user = User::model()->find('temp_old_id=:temp_old_id', array(':temp_old_id'=>$um->user_id));

			$data = new UserMovie;
			$data->movie_id = $movie->id;
			$data->user_id = $user->id;

			if($um->favorite==1) {
				$data->favorite = $um->favorite;
				$data->favorite_register = new CDbExpression('NOW()');
			}

			if(!$data->save())
				$status['errors'][] = array('movie'=>$rate->movie_id, 'user'=>$rate->user_id, 'messages'=>$data->errors);
		}

		$this->jsonResponse($status);
	}

	public function actionRates()
	{
		Rate::model()->deleteAll();
		$rates = OldRate::model()->findAll();
		$status = array('total'=>count($rates), 'errors'=>array());
		Rate::model()->deleteAll();

		foreach($rates as $rate) {
			$user = User::model()->find('temp_old_id=:temp_old_id', array('temp_old_id'=>$rate->user_id));
			$movie = Movie::model()->find('temp_old_id=:temp_old_id', array('temp_old_id'=>$rate->movie_id));

			$data = new Rate;
			$data->rating = $rate->value;
			$data->user_id = $user->id;
			$data->movie_id = $movie->id;
			$data->comment = isset($rate->comment->description) ? $rate->comment->description : NULL;
			$data->register = $rate->date;

			if(!$data->save())
				$status['errors'][] = array('movie'=>$rate->movie_id, 'user'=>$rate->user_id, 'messages'=>$data->errors);
		}

		$this->jsonResponse($status);
	}
}