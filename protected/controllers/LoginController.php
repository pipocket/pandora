<?php

class LoginController extends Controller
{
	public function actionIndex()
	{
		$googleAccount = new GoogleAccount;

		// Get access token
		if (Yii::app()->request->getParam('code')) {
			try {
				$googleAccount->authenticate();
			}
			catch( Exception $e ) {
				$this->redirect( $this->createAbsoluteUrl('/login') );
			}
		}

		if ($googleAccount->token() AND $googleAccount->userinfo) {
			$googleAccount->setAccessToken();

			// Create/Update user, and log this
			$user = AdminUser::model()->addOrUpdate($googleAccount->userinfo);

			// Get user id
			$googleAccount->userinfo->id = $user->id;
			// Picture
			$googleAccount->userinfo->picture = $user->picture;

			// Set user
			Yii::app()->session['user'] = $googleAccount->userinfo;

			// Register login
			$googleAccount->register($user->email);

			// Go to home
			$this->redirect( $this->createAbsoluteUrl('/') );
		}
		else {
			$this->redirect( $googleAccount->oAuthUrl() );
		}
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}
}