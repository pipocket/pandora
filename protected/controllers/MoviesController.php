<?php

class MoviesController extends Controller
{
	public function actionIndex()
	{
		$movies = Movie::model()->findAll();
		$this->render('index', array('movies'=>$movies));
	}

	public function actionDetail($id=null)
	{
		$this->renderPartial('detail', array(
			'movie'=>$this->loadModel($id), 
			'genres'=>CHtml::listData(Genre::model()->findAll(), 'id', 'name'),
			'countries'=>CHtml::listData(Country::model()->findAll(), 'id', 'name'),
			'directors'=>CHtml::listData(Cast::model()->directorsOnly()->findAll(), 'id', 'name'),
			'actors'=>CHtml::listData(Cast::model()->actorsOnly()->findAll(), 'id', 'name'),
			)
		);
	}

	public function actionList()
	{
		$movies = Movie::model()->findAll();
		$this->renderPartial('list', array('movies'=>$movies));
	}

	public function actionItemList($id)
	{
		$model = $this->loadModel($id);
		$this->renderPartial('item-list', array('movie'=>$model));
	}

	public function actionSave()
	{
		$return = array();
		$id = Yii::app()->request->getParam('id');
		$ajax = Yii::app()->request->getParam('ajax');
		$model = $this->loadModel($id);

		if(isset($_POST['Movie'])) {
			if(!$id)
				$_POST['Movie']['author_id'] = 1;

			if(isset($_POST['action'])) {
				if($_POST['action']=='send-to-approve')
					$_POST['Movie']['status'] = 2;
				else if($_POST['action']=='publish') {
					$_POST['Movie']['status'] = 3;
					$_POST['Movie']['publish_in'] = new CDbExpression('NOW()');
				}
				else if($_POST['action']=='delete')
					$_POST['Movie']['status'] = 0;
			}

			$model->attributes = $_POST['Movie'];

			if($model->validate()) {
				$return['status'] = $model->save();

				// Relations
				MovieCast::model()->checkAndSave($_POST['MovieCast'], $model->id);
				MovieGenre::model()->checkAndSave($_POST['MovieGenre'], $model->id);
				MovieCountry::model()->checkAndSave($_POST['MovieCountry'], $model->id);

				if(isset($_POST['new_file']) AND $_POST['new_file']=='true') {
					$file = new File;
					$fileNames = array(
						'default'		=> 'movie-'.$model->id,
						'medium' 		=> 'movie-'.$model->id.'-medium',
						'small' 		=> 'movie-'.$model->id.'-small',
						'facebook' 		=> 'movie-'.$model->id.'-fb_90x90'
					);
					
					// Move from tmp
					$response = $file->copy(
						array(
							'fileName' => $model->poster,
							'newFileName' => $fileNames['default'],
							'moveFrom' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
							'moveTo' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
							'removeOriginalFile' => true
						)
					);

					// Update file name
					$fileNames['default'] = $file->fileName;

					if($response) {
						// Generate medium poster
						$file->copy(
							array(
								'fileName' => $fileNames['default'],
								'newFileName' => $fileNames['medium'],
								'moveFrom' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'moveTo' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'resizeImageTo' => array('width'=>200, 'height'=>300),
								'removeOriginalFile' => false
							)
						);
						$fileNames['medium'] = $file->fileName;

						// Generate small poster
						$file->copy(
							array(
								'fileName' => $fileNames['default'],
								'newFileName' => $fileNames['small'],
								'moveFrom' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'moveTo' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'resizeImageTo' => array('width'=>146, 'height'=>216),
								'removeOriginalFile' => false,
							)
						);
						$fileNames['small'] = $file->fileName;

						// Generate facebook poster (90x90)
						$file->copy(
							array(
								'fileName' => $fileNames['default'],
								'newFileName' => $fileNames['facebook'],
								'moveFrom' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'moveTo' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
								'resizeImageTo' => array('width'=>61, 'height'=>90),
								'removeOriginalFile' => false,
								'watermark' => true,
								'watermark_image' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'images' . DS . 'fb_background_canvas.jpg',
							)
						);
						$fileNames['facebook'] = $file->fileName;

						// Send to Aws S3
						if(Yii::app()->params['aws']['use']) {
							$Aws = new Aws();

							foreach($fileNames as $fn) {
								$st = $Aws->send(Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS . $fn, $fn);

								if($st)
									@unlink(Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS . $fn);
							}
						}

						Movie::model()->updateByPk($model->id, array('poster'=>$fileNames['default']));
					}
				}

				$return['info'] = array(
					'id'=>$model->id,
					'url'=>$this->createUrl('/movies/detail/'.$model->id),
					'itemUrl'=>$this->createUrl('/movies/itemlist/'.$model->id),
				);
			}
			else {
				$return['status'] = false;
				$return['info'] = $model->errors;
			}

			if($ajax)
				echo CJavaScript::jsonEncode($return);
			else
				return var_dump($return);
		}
	}

	public function getCast($data)
	{
		$criteria = new CDbCriteria;
		$ids = array();

		foreach($data as $key => $item)
			$criteria->compare('id', $item->cast_id);

		return Cast::model()->findAll($criteria);
	}

	public function loadModel($id)
	{
		$model = Movie::model()->findByPk($id);
		
		if($model===null)
			$model = new Movie;
		
		return $model;
	}

	public function actionUpload()
	{
		$file = new File;
		$response = $file->upload(
				array(
					'requestName' => 'file',
					'moveTo' => Yii::app()->params['basePath'] . DS . 'public' . DS . 'movies' . DS,
					'isImage' => true,
					'extensions' => array('jpg', 'jpeg'),
					'imageMinWidth' => 440,
					'resizeImageTo' => array('width'=>440, 'height'=>660),
					'returnImageContent' => true
				)
			);

		$this->jsonResponse($response);
	}

	public function actionCheckslug()
	{
		$data = Movie::model()->checkSlug(Yii::app()->request->getPost('name'), Yii::app()->request->getPost('id'));
		$this->jsonResponse($data);
	}
}