<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $password
 * @property string $city
 * @property string $gender
 * @property string $birthdate
 * @property string $facebook
 * @property string $twitter
 * @property string $avatar
 * @property string $register
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Discussion[] $discussions
 * @property Rate[] $rates
 * @property Cast[] $casts
 * @property Genre[] $genres
 * @property UserLog[] $userLogs
 * @property Movie[] $movies
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, password, register', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('name, username, email, city, facebook, twitter, avatar', 'length', 'max'=>125),
			array('password', 'length', 'max'=>255),
			array('gender', 'length', 'max'=>15),
			array('birthdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, username, email, password, city, gender, birthdate, facebook, twitter, avatar, register, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'discussions' => array(self::HAS_MANY, 'Discussion', 'user_id'),
			'rates' => array(self::HAS_MANY, 'Rate', 'user_id'),
			'casts' => array(self::MANY_MANY, 'Cast', 'user_cast(user_id, cast_id)'),
			'genres' => array(self::MANY_MANY, 'Genre', 'user_genre(user_id, genre_id)'),
			'userLogs' => array(self::HAS_MANY, 'UserLog', 'user_id'),
			'movies' => array(self::MANY_MANY, 'Movie', 'user_movie(user_id, movie_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'username' => 'Username',
			'email' => 'Email',
			'password' => 'Password',
			'city' => 'City',
			'gender' => 'Gender',
			'birthdate' => 'Birthdate',
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'avatar' => 'Avatar',
			'register' => 'Register',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}