<?php

/**
 * This is the model class for table "admin_log".
 *
 * The followings are the available columns in table 'admin_log':
 * @property integer $id
 * @property string $action
 * @property string $register
 * @property integer $admin_user_id
 *
 * The followings are the available model relations:
 * @property AdminUser $adminUser
 */
class AdminLog extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdminLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('action, register, admin_user_id', 'required'),
			array('admin_user_id', 'numerical', 'integerOnly'=>true),
			array('action', 'length', 'max'=>125),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, action, register, admin_user_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminUser' => array(self::BELONGS_TO, 'AdminUser', 'admin_user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'action' => 'Action',
			'register' => 'Register',
			'admin_user_id' => 'Admin User',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('admin_user_id',$this->admin_user_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function add($action, $userId)
	{
		$model = new AdminLog;
		$model->action = $action;
		$model->register = new CDbExpression('NOW()');
		$model->admin_user_id = $userId;

		return $model->save();
	}
}