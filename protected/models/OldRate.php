<?php

/**
 * This is the model class for table "rate".
 *
 * The followings are the available columns in table 'rate':
 * @property integer $id
 * @property integer $value
 * @property integer $user_id
 * @property integer $movie_id
 * @property string $foursquare_checkin_id
 * @property string $foursquare_checkin_place
 * @property string $date
 * @property string $ip
 *
 * The followings are the available model relations:
 * @property Comment $comment
 * @property Movie $movie
 * @property User $user
 */
class OldRate extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OldRate the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rate';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('value, user_id, movie_id, date', 'required'),
			array('value, user_id, movie_id', 'numerical', 'integerOnly'=>true),
			array('foursquare_checkin_id', 'length', 'max'=>255),
			array('foursquare_checkin_place', 'length', 'max'=>125),
			array('ip', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, value, user_id, movie_id, foursquare_checkin_id, foursquare_checkin_place, date, ip', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'comment' => array(self::HAS_ONE, 'OldComment', 'rate_id'),
			'movie' => array(self::BELONGS_TO, 'Movie', 'movie_id'),
			'user' => array(self::BELONGS_TO, 'User', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'value' => 'Value',
			'user_id' => 'User',
			'movie_id' => 'Movie',
			'foursquare_checkin_id' => 'Foursquare Checkin',
			'foursquare_checkin_place' => 'Foursquare Checkin Place',
			'date' => 'Date',
			'ip' => 'Ip',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('value',$this->value);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('foursquare_checkin_id',$this->foursquare_checkin_id,true);
		$criteria->compare('foursquare_checkin_place',$this->foursquare_checkin_place,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('ip',$this->ip,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}