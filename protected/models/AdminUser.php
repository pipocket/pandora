<?php

/**
 * This is the model class for table "admin_user".
 *
 * The followings are the available columns in table 'admin_user':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $google_id
 * @property string $picture
 * @property integer $status
 * @property string $register
 *
 * The followings are the available model relations:
 * @property AdminLog[] $adminLogs
 * @property AdminPermission[] $adminPermissions
 */
class AdminUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AdminUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, email, google_id', 'required'),
			array('name', 'length', 'max'=>45),
			array('email, google_id, picture', 'length', 'max'=>125),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, email, google_id, picture, status, register', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminLogs' => array(self::HAS_MANY, 'AdminLog', 'admin_user_id'),
			'adminPermissions' => array(self::MANY_MANY, 'AdminPermission', 'admin_user_permission(admin_user_id, admin_permission_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'google_id' => 'Google ID',
			'picture' => 'Picture',
			'status' => 'Status',
			'register' => 'Register',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('google_id',$this->google_id,true);
		$criteria->compare('picture',$this->picture,true);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('register',$this->register,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function addOrUpdate($userinfo)
	{
		$model = AdminUser::model()->find('email=:email', array(':email'=>$userinfo->email));
		$logAction = 'login';

		if (!$model) {
			$model = new AdminUser;
		}

		$model->name = $userinfo->name;
		$model->email = $userinfo->email;
		$model->google_id = $userinfo->google_id;

		// Se imagem existe no google, mas não na base
		if(!$model->picture AND $userinfo->picture)
			$model->picture = $userinfo->picture;

		// Novo usuário
		if (!$model->register) {
			$model->register = new CDbExpression('NOW()');
			$logAction = 'register';
		}

		// Log
		if ($model->save()) {
			AdminLog::model()->add($logAction, $model->id);
		}

		return $model;
	}
}