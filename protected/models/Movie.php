<?php

/**
 * This is the model class for table "movie".
 *
 * The followings are the available columns in table 'movie':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property string $slug
 * @property integer $age
 * @property integer $time
 * @property integer $year
 * @property string $review
 * @property integer $review_status
 * @property string $release
 * @property string $poster
 * @property integer $rating
 * @property integer $evaluated
 * @property integer $favorited
 * @property integer $commented
 * @property integer $author_id
 * @property string $publish
 * @property integer $status
 * @property string $register
 *
 * The followings are the available model relations:
 * @property Cast[] $casts
 * @property Country[] $countries
 * @property Genre[] $genres
 * @property Rate[] $rates
 * @property User[] $users
 */
class Movie extends CActiveRecord
{
	public $genreList;
	public $itemStatus;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Movie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, original_name, slug, year, review', 'required'),
			array('age, time, year, review_status, status, author_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>255),
			array('original_name, slug, poster', 'length', 'max'=>125),
			array('release, published_in', 'safe'),
			array('register', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, original_name, slug, age, time, year, review, review_status, release, register, published_in, poster, status, author_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			// 'casts' => array(self::MANY_MANY, 'Cast', 'movie_cast(movie_id, cast_id)'),
			'actors' => array(self::HAS_MANY, 'MovieCast', 'movie_id', 'scopes'=>array('actors')),
			'directors' => array(self::HAS_MANY, 'MovieCast', 'movie_id', 'scopes'=>array('directors')),
			'countries' => array(self::MANY_MANY, 'Country', 'movie_country(movie_id, country_id)'),
			'genres' => array(self::MANY_MANY, 'Genre', 'movie_genre(movie_id, genre_id)'),
			// 'rates' => array(self::HAS_MANY, 'Rate', 'movie_id'),
			// 'users' => array(self::MANY_MANY, 'User', 'user_movie(movie_id, user_id)'),
			'author' => array(self::BELONGS_TO, 'AdminUser', 'author_id'),
		);
	}

	public function defaultScope()
	{
		$criteria = array(
			'condition'=>'status > 0',
			'limit'=>'20',
			'order'=>'id DESC'
		);

		// Filter
		if(isset($_GET['term']) AND $_GET['term']) {
			$term = $_GET['term'];
			$criteria['condition'] .= ' AND (name LIKE "%'. $term .'%" OR original_name LIKE "%'. $term .'%")';
		}
		if(isset($_GET['filter'])) {
			foreach($_GET['filter'] as $filter) {
				if($filter=='no-poster')
					$criteria['condition'] .= ' AND poster is null';
				if($filter=='incomplete')
					$criteria['condition'] .= ' AND (age is null OR time is null OR `release` is null)';
				if($filter=='awaiting-approval')
					$criteria['condition'] .= ' AND (status=2)';
			}
		}

		// Pagination
		if(isset($_GET['page'])) {
			$criteria['offset'] = (($_GET['page']-1)*$criteria['limit']);
		}

		return $criteria;
	}

	public function afterFind()
	{
		if(!$this->age OR !$this->time OR !$this->release)
			$this->itemStatus = 'incomplete';
		else if($this->status==2)
			$this->itemStatus = 'publish';
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'original_name' => 'Original Name',
			'slug' => 'Slug',
			'age' => 'Age',
			'time' => 'Time',
			'year' => 'Year',
			'review' => 'Review',
			'review_status' => 'Review Status',
			'release' => 'Release',
			'register' => 'Register',
			'published_in' => 'Published In',
			'poster' => 'Poster',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('age',$this->age);
		$criteria->compare('time',$this->time);
		$criteria->compare('year',$this->year);
		$criteria->compare('review',$this->review,true);
		$criteria->compare('review_status',$this->review_status);
		$criteria->compare('release',$this->release,true);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('published_in',$this->published_in,true);
		$criteria->compare('poster',$this->poster,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function verifySlug($attribute, $params)
	{
		if($this->checkSlug($this->{$attribute}, $this->id))
			$this->addError($attribute, 'Invalid slug');
	}

	public function checkSlug($name, $id)
	{
		$status = true;
		$slug = $this->generateSlug($name);
		
		$criteria = new CDbCriteria;
		$criteria->condition = 'slug=:slug';
		$criteria->params = array(':slug'=>$slug);

		if ($id) {
			$criteria->condition .= ' AND id<>:id';
			$criteria->params[':id'] = $id;
		}

		if((int)self::model()->count($criteria) > 0)
			$status = false;

		return array('status'=>$status, 'slug'=>$slug);
	}

	private function generateSlug($str, $replace=array(), $delimiter='-') {
		setlocale(LC_ALL, 'en_US.UTF8');

		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}