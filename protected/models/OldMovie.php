<?php

/**
 * This is the model class for table "movie".
 *
 * The followings are the available columns in table 'movie':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property string $age
 * @property string $time
 * @property string $storyline
 * @property string $year
 * @property string $origin
 * @property integer $rate
 * @property string $key
 * @property string $preview
 * @property string $release
 * @property string $date
 * @property integer $status
 * @property integer $storyline_check
 * @property integer $revision
 *
 * The followings are the available model relations:
 * @property HistoryUserNextMovie[] $historyUserNextMovies
 * @property Actor[] $actors
 * @property Director[] $directors
 * @property Gender[] $genders
 * @property MoviePoster[] $moviePosters
 * @property MovieThumb[] $movieThumbs
 * @property Rate[] $rates
 * @property User[] $users
 * @property User[] $users1
 */
class OldMovie extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OldMovie the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, original_name, age, time, storyline, year, origin, key, release, date', 'required'),
			array('rate, status, storyline_check, revision', 'numerical', 'integerOnly'=>true),
			array('name, original_name, key', 'length', 'max'=>125),
			array('age', 'length', 'max'=>50),
			array('time', 'length', 'max'=>10),
			array('year', 'length', 'max'=>4),
			array('origin', 'length', 'max'=>45),
			array('preview', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, original_name, age, time, storyline, year, origin, rate, key, preview, release, date, status, storyline_check, revision', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'historyUserNextMovies' => array(self::HAS_MANY, 'HistoryUserNextMovie', 'movie_id'),
			'actors' => array(self::MANY_MANY, 'OldActor', 'movie_actor(movie_id, actor_id)'),
			'directors' => array(self::MANY_MANY, 'OldDirector', 'movie_director(movie_id, director_id)'),
			'genders' => array(self::MANY_MANY, 'OldGender', 'movie_gender(movie_id, gender_id)'),
			'moviePosters' => array(self::HAS_MANY, 'MoviePoster', 'movie_id'),
			'movieThumbs' => array(self::HAS_MANY, 'MovieThumb', 'movie_id'),
			'rates' => array(self::HAS_MANY, 'Rate', 'movie_id'),
			'users' => array(self::MANY_MANY, 'User', 'user_movie(movie_id, user_id)'),
			'users1' => array(self::MANY_MANY, 'User', 'user_next_movie(movie_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'original_name' => 'Original Name',
			'age' => 'Age',
			'time' => 'Time',
			'storyline' => 'Storyline',
			'year' => 'Year',
			'origin' => 'Origin',
			'rate' => 'Rate',
			'key' => 'Key',
			'preview' => 'Preview',
			'release' => 'Release',
			'date' => 'Date',
			'status' => 'Status',
			'storyline_check' => 'Storyline Check',
			'revision' => 'Revision',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('age',$this->age,true);
		$criteria->compare('time',$this->time,true);
		$criteria->compare('storyline',$this->storyline,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('origin',$this->origin,true);
		$criteria->compare('rate',$this->rate);
		$criteria->compare('key',$this->key,true);
		$criteria->compare('preview',$this->preview,true);
		$criteria->compare('release',$this->release,true);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('storyline_check',$this->storyline_check);
		$criteria->compare('revision',$this->revision);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}