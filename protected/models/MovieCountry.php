<?php

/**
 * This is the model class for table "movie_country".
 *
 * The followings are the available columns in table 'movie_country':
 * @property integer $movie_id
 * @property integer $country_id
 */
class MovieCountry extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MovieCountry the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'movie_country';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('movie_id, country_id', 'required'),
			array('movie_id, country_id', 'numerical', 'integerOnly'=>true),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('movie_id, country_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'movie_id' => 'Movie',
			'country_id' => 'Country',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('movie_id',$this->movie_id);
		$criteria->compare('country_id',$this->country_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function checkAndSave($countries, $movie_id, $by='name')
	{
		// Remove old
		MovieCountry::model()->deleteAll('movie_id=:movie_id', array(':movie_id'=>$movie_id));

		$countries = explode(',', $countries);

		foreach($countries as $country) {
			$data = Country::model()->checkAndSave($country, $by);

			$movie_country = new MovieCountry;
			$movie_country->movie_id = $movie_id;
			$movie_country->country_id = $data->id;
			$movie_country->save();
		}
	}
}