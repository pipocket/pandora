<?php

/**
 * This is the model class for table "cast".
 *
 * The followings are the available columns in table 'cast':
 * @property integer $id
 * @property string $name
 * @property string $original_name
 * @property string $slug
 * @property string $about
 * @property string $birthday
 * @property string $city
 * @property string $state
 * @property string $country
 * @property string $photo
 * @property integer $actor
 * @property integer $director
 * @property string $register
 * @property integer $status
 *
 * The followings are the available model relations:
 * @property Movie[] $movies
 * @property User[] $users
 */
class Cast extends CActiveRecord
{
	public $selected = false;

	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cast the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cast';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, register', 'required'),
			array('actor, director, status', 'numerical', 'integerOnly'=>true),
			array('name, original_name, slug, city, state, country, photo', 'length', 'max'=>125),
			array('register', 'default', 'value'=>new CDbExpression('NOW()'), 'setOnEmpty'=>false,'on'=>'insert'),
			array('birthday', 'dateFormat'),
			array('slug', 'generateSlug'),
			array('about, birthday', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, original_name, slug, about, birthday, city, state, country, photo, actor, director, register, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'movies' => array(self::MANY_MANY, 'Movie', 'movie_cast(cast_id, movie_id)'),
			'users' => array(self::MANY_MANY, 'User', 'user_cast(cast_id, user_id)'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'original_name' => 'Original Name',
			'slug' => 'Slug',
			'about' => 'About',
			'birthday' => 'Birthday',
			'city' => 'City',
			'state' => 'State',
			'country' => 'Country',
			'photo' => 'Photo',
			'actor' => 'Actor',
			'director' => 'Director',
			'register' => 'Register',
			'status' => 'Status',
		);
	}

	public function scopes()
	{
		return array(
			'actors'=>array('condition'=>'act.actor=1', 'select'=>'name', 'alias'=>'act'),
			'actorsOnly'=>array('condition'=>'actor=1'),
			'directors'=>array('condition'=>'dct.director=1', 'select'=>'name', 'alias'=>'dct'),
			'directorsOnly'=>array('condition'=>'director=1'),
			'active'=>array('condition' => 'status = 1'),
			'inactive'=>array('condition' => 'status = 0'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('original_name',$this->original_name,true);
		$criteria->compare('slug',$this->slug,true);
		$criteria->compare('about',$this->about,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('actor',$this->actor);
		$criteria->compare('director',$this->director);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function generateSlug($attribute, $params)
	{
		$this->$attribute = $this->slug($this->name);
	}

	public function slug($str, $replace=array(), $delimiter='-') {
		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("/[^a-zA-Z0-9\/_|+ -]/", '', $clean);
		$clean = strtolower(trim($clean, '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}

	public function dateFormat($attribute, $params)
	{
		if(!$this->hasErrors() AND $this->{$attribute} != '') {
			$date = $this->{$attribute};
			$date = explode('/', $date);
			$date = array_reverse($date);
			$date = implode('-', $date);
			
			// return in format Y-m-d
			$this->{$attribute} = $date;
		}
		else if($this->{$attribute} == '')
			$this->{$attribute} = NULL;
	}

	public function checkAndSave($name, $type)
	{
		$name = trim($name);
		$data = Cast::model()->find('name=:name', array(':name'=>$name));

		if ($data) {
			Cast::model()->updateByPk($data->id, array($type=>1));
		}
		else {
			$data = New Cast;
			$data->name = $name;
			$data->slug = $this->slug($name);
			$data->register = new CDbExpression('NOW()');
			$data->$type = 1;
			$data->save();
		}

		return $data;
	}
}