<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $name
 * @property string $email
 * @property string $gender
 * @property string $birthdate
 * @property string $location
 * @property string $facebook
 * @property string $twitter
 * @property string $twitter_oauth_token
 * @property string $twitter_oauth_token_secret
 * @property string $foursquare
 * @property string $password
 * @property string $last_login
 * @property string $last_ip
 * @property string $last_mode
 * @property integer $points
 * @property string $register
 * @property integer $status
 * @property integer $security
 *
 * The followings are the available model relations:
 * @property AdminUser[] $adminUsers
 * @property Feedback[] $feedbacks
 * @property HistoryUserNextMovie[] $historyUserNextMovies
 * @property Rate[] $rates
 * @property Actor[] $actors
 * @property Director[] $directors
 * @property Gender[] $genders
 * @property Movie[] $movies
 * @property UserPreference $userPreference
 */
class OldUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return OldUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->db2;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, password, last_login, last_mode, register', 'required'),
			array('points, status, security', 'numerical', 'integerOnly'=>true),
			array('name, email, facebook, twitter, foursquare', 'length', 'max'=>255),
			array('gender', 'length', 'max'=>1),
			array('location, twitter_oauth_token, twitter_oauth_token_secret, password', 'length', 'max'=>125),
			array('last_ip', 'length', 'max'=>20),
			array('last_mode', 'length', 'max'=>25),
			array('birthdate', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, email, gender, birthdate, location, facebook, twitter, twitter_oauth_token, twitter_oauth_token_secret, foursquare, password, last_login, last_ip, last_mode, points, register, status, security', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'adminUsers' => array(self::HAS_MANY, 'AdminUser', 'user_id'),
			'feedbacks' => array(self::HAS_MANY, 'Feedback', 'user_id'),
			'historyUserNextMovies' => array(self::HAS_MANY, 'HistoryUserNextMovie', 'user_id'),
			'rates' => array(self::HAS_MANY, 'Rate', 'user_id'),
			'actors' => array(self::MANY_MANY, 'Actor', 'user_actor(user_id, actor_id)'),
			'directors' => array(self::MANY_MANY, 'Director', 'user_director(user_id, director_id)'),
			'genders' => array(self::MANY_MANY, 'Gender', 'user_gender(user_id, gender_id)'),
			'movies' => array(self::MANY_MANY, 'Movie', 'user_next_movie(user_id, movie_id)'),
			'userPreference' => array(self::HAS_ONE, 'UserPreference', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'email' => 'Email',
			'gender' => 'Gender',
			'birthdate' => 'Birthdate',
			'location' => 'Location',
			'facebook' => 'Facebook',
			'twitter' => 'Twitter',
			'twitter_oauth_token' => 'Twitter Oauth Token',
			'twitter_oauth_token_secret' => 'Twitter Oauth Token Secret',
			'foursquare' => 'Foursquare',
			'password' => 'Password',
			'last_login' => 'Last Login',
			'last_ip' => 'Last Ip',
			'last_mode' => 'Last Mode',
			'points' => 'Points',
			'register' => 'Register',
			'status' => 'Status',
			'security' => 'Security',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('gender',$this->gender,true);
		$criteria->compare('birthdate',$this->birthdate,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('facebook',$this->facebook,true);
		$criteria->compare('twitter',$this->twitter,true);
		$criteria->compare('twitter_oauth_token',$this->twitter_oauth_token,true);
		$criteria->compare('twitter_oauth_token_secret',$this->twitter_oauth_token_secret,true);
		$criteria->compare('foursquare',$this->foursquare,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('last_login',$this->last_login,true);
		$criteria->compare('last_ip',$this->last_ip,true);
		$criteria->compare('last_mode',$this->last_mode,true);
		$criteria->compare('points',$this->points);
		$criteria->compare('register',$this->register,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('security',$this->security);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}