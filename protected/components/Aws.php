<?php
// Load AWS SDK (V2)
require dirname(__FILE__) . DS . 'aws-sdk' . DS . 'aws.phar';

// Load namespaces
use Aws\S3\Enum\CannedAcl;
use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Aws extends CComponent
{
	public function send($file, $object)
	{
		$s3 = S3Client::factory(array(
			'key'    => Yii::app()->params['aws']['key'],
			'secret' => Yii::app()->params['aws']['secret'],
		));

		try {
			$r = $s3->putObject(array(
				'Bucket'=> Yii::app()->params['aws']['bucket'],
				'Key'	=> $object,
				'Body'	=> fopen($file, 'r'),
				'ACL'	=> CannedAcl::PUBLIC_READ
			));

			return (object)array('status'=>true);
		} catch(S3Exception $e) {
			echo $e->getMessage();
			return (object)array('status'=>false, 'message'=>$e->getMessage());
		}
	}
}