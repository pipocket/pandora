<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
    public function authenticate()
    {
        $user = AdminUser::model()->find('email=:email', array(':email'=>$this->username));

        if ($user===null) {
            $this->errorCode=self::ERROR_USERNAME_INVALID;
        }
        elseif ($user->status==0) {
            $this->errorCode=self::ERROR_BLOCKED_ACCOUNT;
        }
        else {
            // Controller
            $controller = Yii::app()->controller;
            $this->errorCode=self::ERROR_NONE;
        }

        return $this->errorCode;
    }
}