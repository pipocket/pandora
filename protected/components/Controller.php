<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();
	public $user;

	public function init()
	{
		parent::init();
		Yii::app()->language = 'pt-br';
		$this->user = Yii::app()->session['user'];
	}

	public function dateFormat($date, $format='dd/MM/y')
	{
		return Yii::app()->dateFormatter->format($format, strtotime($date));
	}

	public function generateList($data, $field, $relation = null)
	{
		$list = array();

		foreach($data as $item) {
			if(!$relation)
				array_push($list, $item->$field);
			else
				array_push($list, $item->$relation->$field);
		}

		return implode(',', $list);
	}

	public function defaultValue($data, $field, $default)
	{
		return $data->$field ? $data->$field : $default;
	}

	public function jsonResponse($data)
	{
		header('Cache-Control: no-cache, must-revalidate');
		header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
		header('Content-type: application/json');
		echo CJavaScript::jsonEncode( $data );
	}

	public function getImage($image, $length='')
	{
		return $length ? str_replace('.', "-{$length}.", $image) : $image;
	}
}