<?php
	class Pagination extends CComponent
	{
		private $pages;
		private $prev;
		private $next;
		public $page;
		public $limit;
		public $offset;
		public $data;
		
		public function __construct()
		{
			if(!$this->page)
				$this->page = isset($_GET['page']) ? $_GET['page'] : 1;

			$this->limit = Yii::app()->params['paginationLimit'];

			$this->offset = ( $this->page-1 )*$this->limit;
		}

		public function total( $total )
		{
			$this->pages = ceil( $total/$this->limit );
			$this->prev = $this->page == 1 ? 1 : $this->page-1;
			$this->next = $this->page == $this->pages ? $this->page : $this->page+1;

			$this->data = array('page'=>$this->page, 'pages'=>$this->pages, 'prev'=>$this->prev, 'next'=>$this->next);
		}
	}