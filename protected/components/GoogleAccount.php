<?php

class GoogleAccount extends CComponent
{
	protected $client;
    protected $oauth2;
    public $userinfo;

    public function __construct()
    {
        // Imports
        Yii::import('ext.google.src.Google_Client');
        Yii::import('ext.google.src.contrib.Google_Oauth2Service');

        // Start API
        $this->client = new Google_Client();
        $this->client->setApplicationName("Pandora - Pipocket");
        $this->client->setClientId(Yii::app()->params['googleSettings']['client_id']);
        $this->client->setClientSecret(Yii::app()->params['googleSettings']['client_secret']);
        $this->client->setRedirectUri(Yii::app()->params['googleSettings']['redirect_uri']);
        $this->client->setUseObjects(true);
        $this->client->setApprovalPrompt(false);
        $this->client->setScopes(Yii::app()->params['googleSettings']['scope']);

        // Call Services
        $this->oauth2 = new Google_Oauth2Service($this->client);
    }

    public function authenticate()
    {
        // Check code
        $this->client->authenticate(Yii::app()->request->getParam('code'));

        // Get Access Token
        Yii::app()->session['token'] = $this->client->getAccessToken();

        // Set Access Token
        $this->setAccessToken();

        // User data
        $userinfo = $this->oauth2->userinfo->get();

        // Validate user
        if($this->validate($userinfo)) {
            $this->userinfo = (object)array(
                'name'=>$userinfo->name, 
                'email'=>$userinfo->email, 
                'picture'=>$userinfo->picture, 
                'birthday'=>$userinfo->birthday, 
                'gender'=>$userinfo->gender,
                'hd'=>$userinfo->hd,
                'google_id'=>$userinfo->id,
            );
        }
        else {
            unset(Yii::app()->session['token']);
            echo 'Access denied.';
            exit;
        }
    }

    public function oAuthUrl()
    {
        return $this->client->createAuthUrl();
    }

    public function register($email)
    {
        $identity = new UserIdentity($email, null);
        $identity->authenticate();

        if ($identity->errorCode!=0) {
            unset(Yii::app()->session['token']);
            echo 'Access denied.';
            exit;
        }

        Yii::app()->user->login($identity, Yii::app()->params['loginDuration']);
        Yii::app()->session['logged'] = date('Y-m-d H:i:s');
    }

	public function token()
    {
        return Yii::app()->session['token'];
    }

    public function setAccessToken()
    {
        $this->client->setAccessToken(Yii::app()->session['token']);
        Yii::app()->session['token'] = $this->client->getAccessToken();
    }

    private function validate($userinfo)
    {
        $status = true;

        // Only WS Users
        if ( Yii::app()->params['googleSettings']['onlyPipocketUsers'] AND $userinfo->hd != Yii::app()->params['googleSettings']['pipocketDomain'] ) {
            $status = false;
        }
        else {
            $exists = User::model()->find('email=:email', array(':email'=>$userinfo->email));

            if ($exists AND $exists->status == 0) {
                $status = false;
            }
        }

        return $status;
    }
}