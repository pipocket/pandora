<?php
class File extends CComponent
{
	public $fileName;
	private $fileType;

	public function upload($info)
	{
		$fileName = isset($_FILES[$info['requestName']]['name']) ? $_FILES[$info['requestName']]['name'] : false;
		$dataUrl = null;
		$status = true;
		$status_ref = null;
		$image = null;

		if($fileName) {
			$this->fileType = $_FILES[$info['requestName']]['type'];

			$validate = $this->validate($info);

			if($validate->status) {
				if(isset($info['returnImageContent']) AND $info['returnImageContent']) {
					$fileContent = file_get_contents($_FILES[$info['requestName']]['tmp_name']);
					$dataUrl = 'data:' . $this->fileType . ';base64,' . base64_encode($fileContent);
				}

				if(isset($info['imageMinWidth']) AND isset($info['isImage'])) {
					$image = imagecreatefromjpeg($_FILES[$info['requestName']]['tmp_name']);

					if(imagesx($image)<$info['imageMinWidth']) {
						$status = false;
						$status_ref = 'imageMinWidth';
					}
				}

				if($status) {
					$upload = move_uploaded_file(
						$_FILES['file']['tmp_name'],
						$info['moveTo'] . $fileName
					);

					$this->resizeImage($info, $info['moveTo'] . $fileName);

					if(!$upload) {
						$status = false;
						$status_ref = 'moveFile';
					}
				}
			}
			else {
				$status = false;
				$status_ref = $validate->status_ref;
			}
		}
		else {
			$status = false;
			$status_ref = 'undefinedIndex';
		}

		// Get Yii Controller
		$controller = Yii::app()->controller;

		$response = array(
		  'fileName' => $fileName,
		  'fileType' => $this->fileType,
		  'dataUrl' => $dataUrl,
		  'fileUrl' => $controller->createAbsoluteUrl('/public/movies/'.$fileName),
		  'destinationPath' => $info['moveTo'] . $fileName,
		  'status' => $status,
		  'status_ref' => $status_ref,
		  'imageX' => $image ? imagesx($image) : null,
		  'imageY' => $image ? imagesy($image) : null,
		  'imageMinWidth' => isset($info['imageMinWidth']) ? $info['imageMinWidth'] : '',
		  'imageMinHeight' => isset($info['imageMinHeight']) ? $info['imageMinHeight'] : '',
		);
		
		return $response;
	}

	public function copy($info)
	{
		$newFileName = $info['newFileName'] . '.' . $this->getExtension($info['fileName']);
		$status = true;

		if(copy($info['moveFrom'] . $info['fileName'], $info['moveTo'] . $newFileName)) {
			if($info['removeOriginalFile'])
				unlink($info['moveFrom'] . $info['fileName']);

			$this->resizeImage($info, $info['moveTo'] . $newFileName);

			$this->fileName = $newFileName;
		}
		else
			$status = false;

		if(isset($info['watermark'])) {
			if(isset($info['watermark_image'])) {
				$image = imagecreatefromjpeg($info['moveTo'] . $newFileName);
				$background = imagecreatefromjpeg($info['watermark_image']);
				$x = round((imagesx($background) - imagesx($image))/2);

				imagecopymerge($background, $image, $x, 0, 0, 0, $info['resizeImageTo']['width'], $info['resizeImageTo']['height'], 100);
				imagejpeg($background, $info['moveTo'] . $newFileName, 100);
				imagedestroy($background);
			}
		}

		return $status;
	}

	public function resizeImage($info, $fileName, $imageQuality = 100)
	{
		$status = false;

		if(isset($info['resizeImageTo']) AND is_array($info['resizeImageTo']) AND count($info['resizeImageTo'])==2) {
			$image = imagecreatefromjpeg($fileName);
			$width = isset($info['resizeImageTo']['width']) ? $info['resizeImageTo']['width'] : 0;
			$height = isset($info['resizeImageTo']['height']) ? $info['resizeImageTo']['height'] : 0;

			$resizedImage = imagecreatetruecolor($width, $height);
			imagecopyresampled($resizedImage, $image, 0, 0, 0, 0, $width, $height, imagesx($image), imagesy($image));
			$status = imagejpeg($resizedImage, $fileName, 100);
		}

		return $status;
	}

	public function getProportions($fileName)
	{
		$image = imagecreatefromjpeg($fileName);
		$imageX = imagesx($image);
		$imageY = imagesy($image);

		return (object)array('w'=>$imageX, 'h'=>$imageY);
	}

	public function crop($info)
	{
		// Load
		$image = imagecreatefromjpeg($info['path'].$info['fileName']);
		// Create
		$new_image = imagecreatetruecolor($info['temporaryImageW'], $info['temporaryImageH']);

		// Crop from original size
		imagecopyresampled($new_image, $image, 0, 0, $info['cropX'], $info['cropY'], imagesx($image), imagesy($image), imagesx($image), imagesy($image));

		// Generate image
		imagejpeg($new_image, $info['path'].$info['fileName'], 100);

		// Resize //
		$this->resizeImage(
				array('resizeImageTo'=>array('width'=>440, 'height'=>650)),
				$info['path'].$info['fileName']
			);

		// DEBUG
		// header('Content-type: image/jpeg');
		// imagejpeg($new_image, null, 100);

		$response = array(
		  'fileName' => $info['fileName'],
		  'dataUrl' => 'data:' . $info['fileType'] . ';base64,' . base64_encode(file_get_contents($info['path'].$info['fileName'])),
		  'status' => true,
		);
		
		return $response;
	}

	public function getExtension($file)
	{
		$image = explode('.', $file);
		return end($image);
	}

	private function validate($info)
	{
		$status = true;
		$status_ref = null;

		if(isset($info['extensions']) AND is_array($info['extensions'])) {
			$type = explode('/', $this->fileType);
			
			if(isset($type[1]) AND array_search($type[1], $info['extensions'])==NULL) {
				$status = false;
				$status_ref = 'invalidExtension';
			}
			else if (!isset($type[1])) {
				$status = false;
				$status_ref = 'invalidFile';
			}
		}

		return (object)array('status'=>$status, 'status_ref'=>$status_ref);
	}
}