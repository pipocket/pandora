<?php
class Tags extends CWidget
{
	public $fieldName;
	public $selectedItems;
	public $itemField;
	public $itemFieldChildren;
	public $items;
	public $htmlOptions;

	public function init(){}

	public function run()
	{
		$itemField = $this->itemField;
		$itemFieldChildren = $this->itemFieldChildren;
		$currentValues = array();

		$this->htmlOptions['data-items'] = join(',', $this->items);
		$this->htmlOptions['data-fieldname'] = $this->fieldName;

		// Elements
		echo "<div class='tag-box'>";
			echo CHtml::textField($this->randomID(), null, $this->htmlOptions);
			echo "<div class='list-items clearfix'>";
					foreach ($this->selectedItems as $selectedItem) {
						if($this->itemFieldChildren) {
							$currentValues[] = $selectedItem->$itemField->$itemFieldChildren;
							$dataValue = strtolower($selectedItem->$itemField->$itemFieldChildren);
							echo "<span data-value='{$dataValue}'>{$selectedItem->$itemField->$itemFieldChildren} <a href='#' tabindex='-1' rel='removeItem' data-item='{$selectedItem->$itemField->$itemFieldChildren}'>×</a></span>";
						}
						else {
							$currentValues[] = $selectedItem->$itemField;
							$dataValue = strtolower($selectedItem->$itemField);
							echo "<span data-value='{$dataValue}'>{$selectedItem->$itemField} <a href='#' tabindex='-1' rel='removeItem' data-item='{$selectedItem->$itemField}'>×</a></span>";
						}
					}
			echo "</div>";

			// Dropdown items: container
			echo "<ul class='dropdown unstyled'></ul>";
			echo CHtml::hiddenField($this->fieldName, join(',', $currentValues));
		echo "</div>";
	}

	private function randomID() {
		$alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
		$pass = array();
		$alphaLength = strlen($alphabet) - 1;

		for($i = 0; $i < 5; $i++) {
			$n = rand(0, $alphaLength);
			$pass[] = $alphabet[$n];
		}

		return implode($pass);
	}
}