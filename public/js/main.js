(function() {
  var adapter, external, init, links, menu, movieDetail, movieFilter, movieForm, scroll, slug, uploadOrigin, _list, _nav, _top, _w;

  uploadOrigin = '';

  _w = $(window);

  _top = $('#top');

  _nav = $('#nav').width();

  _list = $('#list').width();

  init = function() {
    scroll();
    external();
    adapter();
    movieDetail();
    menu();
    links();
    movieForm();
    movieFilter();
    return _w.resize(function() {
      return adapter();
    });
  };

  window.plugins = function() {
    var addTag, removeTag;
    $('.datepicker').pickadate({
      'format': 'dd/mm/yyyy'
    });
    $(document).on('click', '.list-items a[rel=removeItem]', function(e) {
      var $this;
      $this = $(this);
      removeTag($this.attr('data-item'), $this);
      $this.parent().remove();
      return e.preventDefault();
    });
    $(document).on('click', 'ul.dropdown li a:not(.off)', function(e) {
      var $this;
      $this = $(this);
      addTag($this.html(), $this);
      return e.preventDefault();
    });
    $('.input-tags').keyup(function(e) {
      var $this, dropdown, itemOnList, items, j, key, val;
      $this = $(this);
      dropdown = $this.parent().find('ul.dropdown');
      if (e.which === 40) {
        if (dropdown.find('li.selected').length === 0) {
          dropdown.find('li:eq(0):not(.off)').addClass('selected');
        } else {
          dropdown.find('li.selected').removeClass('selected').next(':not(.off)').addClass('selected');
        }
      } else if (e.which === 38) {
        if (dropdown.find('li.selected').length === 0) {
          dropdown.find('li:last:not(.off)').addClass('selected');
        } else {
          dropdown.find('li.selected').removeClass('selected').prev(':not(.off)').addClass('selected');
        }
      } else if (e.which === 13) {
        if (dropdown.find('li.selected').length > 0) {
          val = dropdown.find('li.selected').text();
        } else if ($this.val().length > 0) {
          val = $this.val();
        }
        addTag(val, $this);
      } else {
        if ($this.val().length > 1) {
          key = $this.val().toLowerCase();
          items = $this.attr('data-items').split(',');
          itemOnList = [];
          j = 0;
          $.each(items, function(i, item) {
            var itemKey;
            itemKey = item.toLowerCase();
            if (itemKey.indexOf(key) >= 0 && j < 2) {
              itemOnList += "<li><a href='#'>" + item + "</a></li>";
              return j++;
            }
          });
          if (itemOnList.length > 0) {
            dropdown.html('').append(itemOnList).show();
          } else {
            dropdown.html('').hide();
          }
        } else {
          dropdown.html('').hide();
        }
      }
      return $this.blur(function() {
        return dropdown.html('').hide();
      });
    });
    addTag = function(val, $this) {
      var dropdown, field, fieldName, input, list, values;
      list = $this.parents('.tag-box').find('.list-items');
      fieldName = $this.parents('.tag-box').find('input.input-tags').attr('data-fieldname');
      field = $this.parents('.tag-box').find("input[name='" + fieldName + "']");
      values = val.split(',');
      $.each(values, function(index, val) {
        var currentItems;
        if (list.find("span[data-value='" + (val.toLowerCase()) + "']").length === 0) {
          currentItems = [];
          if (field.val()) {
            currentItems = field.val().split(',');
          }
          currentItems[currentItems.length] = val;
          field.val(currentItems.join(','));
          return list.append("<span data-value='" + (val.toLowerCase()) + "'>" + val + " <a href='#' tabindex='-1' rel='removeItem' data-item='" + val + "'>×</span>");
        }
      });
      dropdown = $this.parents('.tag-box').find('ul.dropdown');
      input = $this.parents('.tag-box').find('input.input-tags').val('');
      return dropdown.hide();
    };
    return removeTag = function(val, $this) {
      var field, fieldName, newValues;
      fieldName = $this.parents('.tag-box').find('input.input-tags').attr('data-fieldname');
      field = $this.parents('.tag-box').find("input[name='" + fieldName + "']");
      newValues = $.grep(field.val().split(','), function(item) {
        return item !== val;
      });
      return field.val(newValues.join(','));
    };
  };

  scroll = function() {
    var go, list, load;
    go = true;
    list = $('#list');
    $(document).on('click', 'a.load-more', function(e) {
      load(list, $(this));
      return e.preventDefault();
    });
    list.scroll(function() {
      var $this, callback, time;
      $this = $(this);
      time = null;
      if ((document.getElementById('list').offsetHeight + $this.scrollTop()) >= document.getElementById('list').scrollHeight && go) {
        go = false;
        callback = function() {
          return load($this);
        };
        return setTimeout(callback, 500);
      }
    });
    return load = function($this, more) {
      var action, page, text;
      if (more == null) {
        more = null;
      }
      action = $this.attr('data-action');
      page = $this.attr('data-page');
      if (more) {
        text = more.html();
        more.html('').addClass('loading');
      } else {
        $this.find('li:last').addClass('last-item').append($('<span/>').addClass('loading bottom'));
      }
      return $.get(action, 'page=' + page, function(data) {
        if (!$(data).eq(0).hasClass('nothing')) {
          $this.attr('data-page', parseInt(page) + 1);
          go = true;
        }
        list.find('ul').append(data);
        if (more) {
          return more.html(text).removeClass('loading');
        } else {
          return $this.find('li.last-item').removeClass('last-item').find('span.loading').remove();
        }
      });
    };
  };

  external = function() {
    return $(document).on('click', 'a[rel=external]', function(e) {
      window.open($(this).attr('href'));
      return e.preventDefault();
    });
  };

  adapter = function() {
    var newHeight;
    newHeight = _w.height() - _top.height() - 1;
    if (_w.width() < 1280) {
      $('#content').addClass('tablet').css({
        'width': parseInt($('#list').width()) - 70,
        'height': newHeight
      });
    } else {
      $('#content').removeClass('tablet').css({
        'width': (_w.width() - _nav - _list) - 1,
        'height': newHeight
      });
    }
    return $('#list').css({
      'height': newHeight
    });
  };

  menu = function() {
    return $(document).on('click', '#nav a.alternate', function(e) {
      $(this).parent().toggleClass('on');
      return e.preventDefault();
    });
  };

  links = function() {
    return $('[data-link-ajax=true]').bind('click', function(e) {
      var $this;
      $this = $(this);
      $.ajax({
        url: $this.attr('href'),
        success: function(response) {
          return $('[data-container=' + $this.data('target') + ']').html(response);
        }
      });
      return e.preventDefault();
    });
  };

  movieFilter = function() {
    var form;
    form = $('form[name=movieFilter]');
    form.find('input[type=text]').keyup(function() {
      return form.submit();
    });
    form.find('input[type=checkbox]').click(function() {
      return form.submit();
    });
    return $(document).on('submit', 'form[name=movieFilter]', function() {
      var $this, list;
      $this = $(this);
      list = $('#list');
      list.find('ul').html('');
      list.append($('<span/>').addClass('loading top'));
      $.get($this.attr('action'), $this.serialize(), function(data) {
        list.find('ul').html(data);
        list.find('span.loading').remove();
        return list.attr('data-action', $this.attr('action') + '?' + $this.serialize());
      });
      return false;
    });
  };

  movieDetail = function() {
    $(document).on('click', '#list [data-ajax=true]', function(e) {
      var $this;
      $this = $(this);
      $('#list li').removeClass('selected');
      $this.parent().addClass('selected').append($('<span/>').addClass('loading'));
      $.ajax({
        url: $this.attr('href'),
        success: function(response) {
          $('[data-container=content]').html(response);
          $this.parent().find('span.loading').remove();
          return $('#content').addClass('on');
        }
      });
      return e.preventDefault();
    });
    return $(document).on('click', '#content input#cancel', function(e) {
      $('#content').removeClass('on');
      $("#list li").removeClass('selected');
      return e.preventDefault();
    });
  };

  movieForm = function() {
    var action;
    action = null;
    $(document).on('keyup', "[name='Movie[name]']", function() {
      var $this, id, name;
      $this = $(this);
      id = $this.data('id');
      name = $this.val();
      $this.addClass('loading');
      return $.ajax({
        type: 'POST',
        url: $this.data('url-check'),
        data: {
          name: name,
          id: id
        },
        dataType: 'json',
        success: function(response) {
          $this.removeClass('loading');
          if (!response.status) {
            toastr.error("A chave <b>" + _slug + "</b> já esta sendo usada por outro filme.");
            return $this.addClass('error');
          } else {
            $("span.key").html(response.slug);
            $("[name='Movie[slug]']").val(response.slug);
            return $this.removeClass('error');
          }
        }
      });
    });
    $(document).on('click', "[type=button].submitMovieForm", function() {
      var $this, send;
      $this = $(this);
      send = true;
      if ($this.data('confirm-message') !== void 0) {
        if (confirm($this.data('confirm-message'))) {
          if ($this.data('confirm-message2') !== void 0) {
            if (!confirm($this.data('confirm-message2'))) {
              send = false;
            }
          }
        } else {
          send = false;
        }
      }
      if (send) {
        action = $this.attr('id');
        return $('form[name=movieForm]').submit();
      }
    });
    return $(document).on('submit', 'form[name=movieForm]', function() {
      var $this, data;
      $this = $(this);
      data = $this.serialize();
      if (action) {
        data += '&action=' + action;
      }
      $('#content .header').append($('<span/>').addClass('loading'));
      if ($this.data('post-ajax') === true) {
        $.ajax({
          type: 'post',
          dataType: 'json',
          url: $this.attr('action') + '/true',
          data: data,
          success: function(response, status, xhr) {
            if (response.status === false) {
              toastr.error('Preencha os campos corretamente', 'Ocorreu um erro');
              return $.each(response.info, function(item, message) {
                return $this.find("[name='Movie[" + item + "]']").addClass('error');
              });
            } else {
              if (action === 'delete') {
                toastr.success('<b>' + $this.find("[name='Movie[name]']").val() + '</b> deletado com sucesso.');
                $('[data-container=content]').html('');
                return $('#list ul li#movie-' + response.info.id).remove();
              } else {
                toastr.success('<b>' + $this.find("[name='Movie[name]']").val() + '</b> adicionado/alterado com sucesso.');
                $('[data-container=content]').load(response.info.url);
                return $.get(response.info.itemUrl, function(data) {
                  if ($('#list ul li#movie-' + response.info.id).length > 0) {
                    return $('#list ul li#movie-' + response.info.id).html($(data).html());
                  } else {
                    return $('#list ul').prepend($(data));
                  }
                });
              }
            }
          }
        });
        return false;
      }
    });
  };

  window.poster = function() {
    var clickFile;
    clickFile = $('a[rel=clickFile]');
    clickFile.unbind('click');
    clickFile.bind('click', function(e) {
      uploadOrigin = $(this).data('origin');
      $('input[type=file].submitFile').attr({
        'data-show-thumb': 'false'
      }).click();
      return e.preventDefault();
    });
    return document.querySelector('.submitFile').addEventListener('change', function(e) {
      var $this, fd, file, xhr;
      $this = $(this);
      file = this.files[0];
      fd = new FormData();
      fd.append('file', file);
      xhr = new XMLHttpRequest();
      xhr.open('POST', $this.data('upload-url'), true);
      xhr.upload.onprogress = function(e) {
        if (e.lengthComputable) {
          return clickFile.html(Math.round((e.loaded / e.total) * 100) + '%');
        }
      };
      xhr.onload = function() {
        var image, link, response;
        clickFile.html(clickFile.data('default-text'));
        if (this.status === 200) {
          response = JSON.parse(this.response);
          if (response.status) {
            if (uploadOrigin === 'normal-button') {
              $('fieldset.file-in-form').find('a.show').remove();
              link = $('<a/>').attr({
                'href': response.fileUrl,
                'class': 'pure-button pure-button-secondary',
                'rel': 'external'
              }).html('Ver Poster');
              $('fieldset.file-in-form').append(link);
            } else {
              $this.parent().find('img').remove();
              image = $('<img/>').attr('src', response.dataUrl);
              $this.parent().prepend(image);
            }
            $this.val('');
            $("[name='Movie[poster]']").val(response.fileName);
            return $("[name='Movie[poster]']").parent().append($('<input/>').attr({
              'type': 'hidden',
              'name': 'new_file',
              'value': 'true'
            }));
          } else {
            if (response.status_ref === 'imageMinWidth') {
              toastr.error('A imagem deve ter a largura mínima de <b>440px</b> e altura mínima de <b>660px</b>!');
            }
            if (response.status_ref === 'invalidFile') {
              return toastr.error('A imagem enviada não é válida!');
            }
          }
        }
      };
      return xhr.send(fd);
    });
  };

  slug = function(str) {
    var from, i, to, _i, _ref;
    str = str.toLowerCase();
    from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
    to = "aaaaaeeeeeiiiiooooouuuunc------";
    for (i = _i = 0, _ref = str.length; 0 <= _ref ? _i < _ref : _i > _ref; i = 0 <= _ref ? ++_i : --_i) {
      str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }
    return str.replace(/[^a-z0-9]+/g, '-');
  };

  init();

}).call(this);
