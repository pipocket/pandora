uploadOrigin = ''
_w = $(window)
_top = $('#top')
_nav = $('#nav').width()
_list = $('#list').width()

init = ->
	scroll()
	external()
	adapter()
	movieDetail()
	menu()
	links()
	movieForm()
	movieFilter()

	_w.resize(->
		adapter()
	)

# prepare plugins
window.plugins = ->
	# datepicker
	$('.datepicker').pickadate
		'format': 'dd/mm/yyyy'

	# Tags
	$(document).on 'click', '.list-items a[rel=removeItem]', (e) ->
		$this = $ this
		removeTag $this.attr('data-item'), $this
		$this.parent().remove()
		e.preventDefault()

	$(document).on 'click', 'ul.dropdown li a:not(.off)', (e) ->
		$this = $ this
		addTag $this.html(), $this
		e.preventDefault()
	
	$('.input-tags').keyup (e) ->
		$this = $ this
		dropdown = $this.parent().find 'ul.dropdown'

		if e.which == 40
			if dropdown.find('li.selected').length == 0
				dropdown.find('li:eq(0):not(.off)').addClass 'selected'
			else
				dropdown.find('li.selected').removeClass('selected').next(':not(.off)').addClass 'selected'
		else if e.which == 38
			if dropdown.find('li.selected').length == 0
				dropdown.find('li:last:not(.off)').addClass 'selected'
			else
				dropdown.find('li.selected').removeClass('selected').prev(':not(.off)').addClass 'selected'
		else if e.which == 13
			if dropdown.find('li.selected').length > 0
				val = dropdown.find('li.selected').text()
			else if $this.val().length > 0
				val = $this.val()

			addTag val, $this
		else
			if $this.val().length > 1
				key = $this.val().toLowerCase()
				items = $this.attr('data-items').split ','
				itemOnList = []
				j = 0

				$.each items, (i, item) ->
					itemKey = item.toLowerCase()
					if itemKey.indexOf(key) >= 0 and j<2
						itemOnList += "<li><a href='#'>#{item}</a></li>"
						j++

				if itemOnList.length > 0
					dropdown.html('').append(itemOnList).show()
				else
					dropdown.html('').hide()
			else
				dropdown.html('').hide()

		$this.blur ->
			dropdown.html('').hide()

	addTag = (val, $this) ->
		list = $this.parents('.tag-box').find '.list-items'
		fieldName = $this.parents('.tag-box').find('input.input-tags').attr 'data-fieldname'
		field = $this.parents('.tag-box').find "input[name='#{fieldName}']"
		values = val.split ','

		$.each values, (index, val) ->
			if list.find("span[data-value='#{val.toLowerCase()}']").length == 0
				currentItems = []

				if field.val()
					currentItems = field.val().split ','

				currentItems[currentItems.length] = val
				field.val currentItems.join(',')

				list.append "<span data-value='#{val.toLowerCase()}'>#{val} <a href='#' tabindex='-1' rel='removeItem' data-item='#{val}'>×</span>"

		dropdown = $this.parents('.tag-box').find 'ul.dropdown'
		input = $this.parents('.tag-box').find('input.input-tags').val ''

		dropdown.hide()

	removeTag = (val, $this) ->
		fieldName = $this.parents('.tag-box').find('input.input-tags').attr 'data-fieldname'
		field = $this.parents('.tag-box').find "input[name='#{fieldName}']"

		newValues = $.grep field.val().split(','), (item) ->
			item != val

		field.val newValues.join ','

scroll = ->
	go = true
	list = $ '#list'

	$(document).on 'click', 'a.load-more', (e) ->
		load list, $(this)
		e.preventDefault()

	list.scroll ->
		$this = $ this
		time = null

		if (document.getElementById('list').offsetHeight+$this.scrollTop()) >= document.getElementById('list').scrollHeight && go
			go = false

			callback = ->
				load $this

			setTimeout callback, 500

	load = ($this, more=null) ->
		action = $this.attr 'data-action'
		page = $this.attr 'data-page'

		if more
			text = more.html()
			more.html('').addClass 'loading'
		else
			$this.find('li:last').addClass('last-item').append $('<span/>').addClass 'loading bottom'

		$.get action, 'page='+page, (data) ->
			if !$(data).eq(0).hasClass('nothing')
				$this.attr 'data-page', parseInt(page)+1
				go = true

			list.find('ul').append data

			if more
				more.html(text).removeClass 'loading'
			else
				$this.find('li.last-item').removeClass('last-item').find('span.loading').remove()

external = ->
	$(document).on 'click', 'a[rel=external]', (e) ->
		window.open($(this).attr 'href')
		e.preventDefault()

# Adaptive based in Screen Resolution
adapter = ->
	newHeight = _w.height()-_top.height()-1 #1px border

	if _w.width() < 1280
		$('#content').addClass('tablet').css
			'width': parseInt($('#list').width())-70, #1px border
			'height': newHeight
	else
		$('#content').removeClass('tablet').css
			'width': (_w.width()-_nav-_list)-1, #1px border
			'height': newHeight

	$('#list').css
		'height': newHeight

menu = ->
	$(document).on 'click', '#nav a.alternate', (e) ->
		$(this).parent().toggleClass 'on'
		e.preventDefault()

# links
links = ->
	$('[data-link-ajax=true]').bind 'click', (e) ->
		$this = $ this

		$.ajax(
			url: $this.attr('href'),
			success: (response) ->
				$('[data-container='+$this.data('target')+']').html response
		)

		e.preventDefault()

movieFilter = ->
	form = $('form[name=movieFilter]')

	form.find('input[type=text]').keyup ->
		form.submit()
	form.find('input[type=checkbox]').click ->
		form.submit()

	$(document).on 'submit', 'form[name=movieFilter]', ->
		$this = $ this
		list = $ '#list'

		list.find('ul').html ''
		list.append $('<span/>').addClass('loading top')

		$.get $this.attr('action'), $this.serialize(), (data) ->
			list.find('ul').html data
			list.find('span.loading').remove()
			list.attr 'data-action', $this.attr('action')+'?'+$this.serialize()

		return false

movieDetail = ->
	$(document).on 'click', '#list [data-ajax=true]', (e) ->
		$this = $ this
		$('#list li').removeClass 'selected'
		$this.parent().addClass('selected').append $('<span/>').addClass('loading')

		$.ajax(
			url: $this.attr('href'),
			success: (response) ->
				$('[data-container=content]').html response
				$this.parent().find('span.loading').remove()

				$('#content').addClass 'on'
		)

		e.preventDefault()

	$(document).on 'click', '#content input#cancel', (e) ->
		$('#content').removeClass 'on'
		$("#list li").removeClass 'selected'

		e.preventDefault()

movieForm = ->
	action = null

	$(document).on 'keyup', "[name='Movie[name]']", ->
		$this = $ this
		id = $this.data 'id'
		name = $this.val();

		$this.addClass 'loading'

		$.ajax
			type: 'POST',
			url: $this.data('url-check'),
			data: {name: name, id: id},
			dataType: 'json',
			success: (response) ->
				$this.removeClass 'loading'

				if !response.status
					toastr.error "A chave <b>#{_slug}</b> já esta sendo usada por outro filme."
					$this.addClass 'error'
				else
					$("span.key").html response.slug
					$("[name='Movie[slug]']").val response.slug
					$this.removeClass 'error'

	# publish/send-to-approve
	$(document).on 'click', "[type=button].submitMovieForm", ->
		$this = $ this
		send = true

		if $this.data('confirm-message') != undefined
			if confirm $this.data 'confirm-message'
				if $this.data('confirm-message2') != undefined
					if !confirm $this.data 'confirm-message2'
						send = false
			else
				send = false

		if send
			action = $this.attr 'id'
			$('form[name=movieForm]').submit()


	$(document).on 'submit', 'form[name=movieForm]', ->
		$this = $ this
		data = $this.serialize()

		if action
			data += '&action='+action

		# Loading
		$('#content .header').append $('<span/>').addClass('loading')

		if $this.data('post-ajax') is true
			$.ajax(
				type: 'post',
				dataType: 'json',
				url: $this.attr('action') + '/true', # true for ajax-post
				data: data,
				success: (response, status, xhr) ->
					if response.status is false
						toastr.error('Preencha os campos corretamente', 'Ocorreu um erro')

						$.each(response.info, (item, message) ->
							$this.find("[name='Movie["+item+"]']").addClass('error')
						)
					else
						if action is 'delete'
							toastr.success('<b>'+$this.find("[name='Movie[name]']").val()+'</b> deletado com sucesso.')
							$('[data-container=content]').html ''
							$('#list ul li#movie-'+response.info.id).remove()
						else
							toastr.success('<b>'+$this.find("[name='Movie[name]']").val()+'</b> adicionado/alterado com sucesso.')
							$('[data-container=content]').load response.info.url

							$.get response.info.itemUrl, (data) ->
								if $('#list ul li#movie-'+response.info.id).length > 0
									$('#list ul li#movie-'+response.info.id).html( $(data).html() )
								else
									$('#list ul').prepend $ data
			)

			return false

window.poster = ->
	clickFile = $ 'a[rel=clickFile]'
	clickFile.unbind('click')
	clickFile.bind 'click', (e) ->
		uploadOrigin = $(this).data 'origin'
		$('input[type=file].submitFile').attr('data-show-thumb':'false').click()
		e.preventDefault()

	document.querySelector('.submitFile').addEventListener 'change', (e) ->
		$this = $ this
		file = this.files[0]
		fd = new FormData()

		fd.append 'file', file

		xhr = new XMLHttpRequest()
		xhr.open 'POST', $this.data('upload-url'), true

		xhr.upload.onprogress = (e) ->
			if e.lengthComputable
				clickFile.html Math.round((e.loaded / e.total)*100) + '%'

		xhr.onload = ->
			clickFile.html clickFile.data('default-text')

			if this.status is 200
				response = JSON.parse this.response

				if response.status
					if uploadOrigin is 'normal-button'
						$('fieldset.file-in-form').find('a.show').remove()
						link = $('<a/>').attr(
									'href':response.fileUrl, 
									'class':'pure-button pure-button-secondary', 
									'rel': 'external'
								).html 'Ver Poster'

						$('fieldset.file-in-form').append link
					else
						$this.parent().find('img').remove()
						image = $('<img/>').attr 'src', response.dataUrl
						$this.parent().prepend image
						
					$this.val ''

					$("[name='Movie[poster]']").val response.fileName
					$("[name='Movie[poster]']").parent().append $('<input/>').attr('type':'hidden', 'name':'new_file', 'value':'true')
				else
					if response.status_ref is 'imageMinWidth'
						toastr.error 'A imagem deve ter a largura mínima de <b>440px</b> e altura mínima de <b>660px</b>!'
					if response.status_ref is 'invalidFile'
						toastr.error 'A imagem enviada não é válida!'

		xhr.send fd

slug = (str) ->
	str = str.toLowerCase()

	# remove accents, swap ñ for n, etc
	from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;"
	to   = "aaaaaeeeeeiiiiooooouuuunc------"

	for i in [0...str.length]
		str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i))

	return str.replace(/[^a-z0-9]+/g, '-');

# call init function
init()